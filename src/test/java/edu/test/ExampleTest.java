package edu.test;

import edu.driver.DriverSingleton;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.assertj.core.api.Assertions.assertThat;

public class ExampleTest {

    WebDriver driver;

    @BeforeEach
    public void setUp() {
        driver = DriverSingleton.getInstance();
    }

    @Test
    public void enter() {
        // given
        String expectedTitle = "Главная - Конструктор Талантов";
        // when
        driver.get("http://tc-stage05.k8s.iba/");
        WebElement searchField = driver.findElement(By.name("_58_login"));
        searchField.sendKeys("zaitsev@tc.by");
        searchField.sendKeys(Keys.ENTER);
        WebElement searchPasswordField = driver.findElement(By.name("_58_password"));
        searchPasswordField.sendKeys("welcome");
        searchPasswordField.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@class='button-holder ']")).click();
        String actualTitle = driver.getTitle();
        // then
        assertThat(actualTitle).isEqualTo(expectedTitle);
    }

    @Test
    public void login() {
        // given
        String expectedTitle = "Не удалось войти в систему.\n" +
                "Email или пароль введены неверно. Пожалуйста, попробуйте еще раз.";
        // when
        driver.get("http://tc-stage05.k8s.iba/");
        WebElement searchField = driver.findElement(By.name("_58_login"));
        searchField.sendKeys("111");
        WebElement searchPasswordField = driver.findElement(By.name("_58_password"));
        searchPasswordField.sendKeys("welcome");
        searchPasswordField.sendKeys(Keys.ENTER);

        searchField.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@class='button-holder ']")).click();

        String actualTitle = driver.findElement(By.xpath("//div[@class='login-error']")).getText();

        // then
        assertThat(actualTitle).isEqualTo(expectedTitle);
    }


    @Test
    public void password() {
        // given
        String expectedTitle = "Не удалось войти в систему.\n" +
                "Email или пароль введены неверно. Пожалуйста, попробуйте еще раз.";
        // when
        driver.get("http://tc-stage05.k8s.iba/");
        WebElement searchField = driver.findElement(By.name("_58_login"));
        searchField.sendKeys("zaitsev@tc.by");
        WebElement searchPasswordField = driver.findElement(By.name("_58_password"));
        searchPasswordField.sendKeys("111");
        searchPasswordField.sendKeys(Keys.ENTER);

        searchField.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@class='button-holder ']")).click();

        String actualTitle = driver.findElement(By.xpath("//div[@class='login-error']")).getText();

        // then
        assertThat(actualTitle).isEqualTo(expectedTitle);
    }

    @Test
    public void example() {
        // given
        String expectedTitle = "IBA BY";
        // when
        driver.get("https://www.google.com/");
        WebElement searchField = driver.findElement(By.name("q"));
        searchField.sendKeys("IBA");
        searchField.sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//div[@class='rc']//a[1]")).click();
        String actualTitle = driver.getTitle();
        // then
        assertThat(actualTitle).isEqualTo(expectedTitle);
    }




    @AfterEach
    public void tearDown() {
        driver.close();
    }

}
